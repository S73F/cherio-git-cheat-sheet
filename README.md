# GIT Cheat Sheet - Cherio Stefano

## Configurazione Iniziale

- `git config --global user.name "Nome Cognome"`: Configura il nome utente.
- `git config --global user.email "email@esempio.com"`: Configura l'email utente.
- `git config --list`: Mostra tutte le configurazioni di Git.

## Gestione delle Chiavi SSH

- `ssh-keygen -t ed25519`: Genera una nuova chiave SSH usando il tipo ED25519. La chiave pubblica generata si trova all'interno di `C:\Users\nome-utente\.ssh\id_ed25519.pub` e va aggiunta alla sezione SSH Keys di Gitlab per permettere l'autenticazione all'utente.

## Inizializzazione e Clonazione

- `git init`: Inizializza un nuovo repository Git nella cartella corrente.
- `git clone <url>`: Clona un repository remoto nella cartella corrente.
- `git clone <url> <cartella-destinazione>`: Clona un repository remoto nella cartella specificata.

## Ignorare File e Cartelle

- `.gitignore`: File di configurazione utilizzato per specificare file e cartelle da ignorare.

## Gestione dei File e delle Cartelle

### Aggiungere file:

- `git add <file>`: Aggiunge file specifici all'area di staging.
- `git add .`: Aggiunge tutti i file modificati all'area di staging.
- `git add <cartella>`: Aggiunge tutti i file nella cartella specificata all'area di staging.

### Rimuovere file:

- `git rm <file>`: Rimuove un file dalla working directory e dall'indice.
- `git rm -r <cartella>`: Rimuove una cartella e tutto il suo contenuto dalla working directory e dall'indice.

### Spostare o rinominare file:

- `git mv <file-sorgente> <file-destinazione>`: Sposta o rinomina un file.

### Ripristinare file:

- `git restore <file>`: Ripristina il file dall'area di staging o dall'ultimo commit.
- `git restore --staged <file>`: Rimuove il file dall'area di staging.

### Pulire la working directory:

- `git clean -f`: Rimuove i file non tracciati dalla working directory.
- `git clean -fd`: Rimuove i file non tracciati e le directory dalla working directory.
- `git clean -n`: Mostra cosa verrebbe rimosso da git clean senza effettivamente rimuovere nulla.

## Stato e Differenze

- `git status`: Mostra lo stato dei file nel repository.
- `git diff`: Mostra le differenze tra i file non ancora committati e quelli dell'ultimo commit.
- `git diff --staged`: Mostra le differenze tra i file in staging e l'ultimo commit.

## Commit e Log

### Effettuare un commit:

- `git commit -m "messaggio del commit"`: Effettua un commit con un messaggio descrittivo.

### Modificare l'ultimo commit:

- `git commit --amend -m "Nuovo messaggio"`: Modifica l'ultimo commit effettuato sostituendo il messaggio con "Nuovo messaggio".

### Visualizzare la cronologia:

- `git log`: Mostra la cronologia dei commit.
- `git log --oneline`: Mostra la cronologia dei commit in formato compatto.

### Mostrare dettagli di un commit specifico:

- `git show <commit-id>`: Mostra i dettagli e i cambiamenti di un commit specifico.

## Branching e Merging

### Gestione dei branch:

- `git branch`: Elenca tutti i branch locali.
- `git branch <nome-branch>`: Crea un nuovo branch.
- `git branch -d <nome-branch>`: Elimina un branch.

### Passaggio tra branch:

- `git checkout <nome-branch>`: Passa a un branch esistente.
- `git checkout -b <nome-branch>`: Crea e passa a un nuovo branch.

### Unione dei branch:

- `git merge <nome-branch>`: Unisce un branch nel branch corrente.

## Sincronizzazione Remota

### Configurazione del repository remoto:

- `git remote add origin <url>`: Imposta il repository remoto.

### Invio dei cambiamenti:

- `git push -u origin <nome-branch>`: Spinge i cambiamenti al branch remoto e lo imposta come upstream.
- `git push`: Spinge i cambiamenti al branch remoto corrente.

### Recupero dei cambiamenti:

- `git pull`: Recupera e integra i cambiamenti dal repository remoto.
- `git fetch`: Recupera i cambiamenti dal repository remoto senza integrarli nel branch corrente.

## Ripristino e Rebase

### Ripristino:

- `git reset --hard <commit-id>`: Ripristina la working directory e l'indice allo stato di un commit specifico.
- `git reset <file>`: Rimuove un file dall'area di staging.
- `git checkout -- <file>`: Ripristina un file alle modifiche dell'ultimo commit.
- `git revert <commit-id>`: Crea un nuovo commit che rimuove le modifiche apportate da `<commit-id>`.

### Rebase:

- `git rebase <branch>`: Integra le modifiche di un branch sul branch corrente riscrivendo la cronologia.
- `git cherry-pick <commit>`: Applica le modifiche introdotte da un commit specifico nel branch corrente.

## Annotazioni e Tag

- `git tag <nome-tag>`: Crea un nuovo tag leggero.
- `git tag -a <nome-tag> -m "messaggio"`: Crea un tag annotato.
- `git tag <nome-tag> <commit-id>`: Crea un tag leggero per un commit specifico.

## Gestione Temporanea delle Modifiche

- `git stash`: Salva temporaneamente le modifiche non commitate.
- `git stash pop`: Applica le modifiche salvate con stash e le rimuove dalla lista degli stash.
